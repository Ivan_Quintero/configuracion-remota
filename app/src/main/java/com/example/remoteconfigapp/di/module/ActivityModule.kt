package com.example.remoteconfigapp.di.module

import com.example.remoteconfigapp.screens.main.view.MainActivity
import com.example.remoteconfigapp.screens.splash.view.SplashActivity
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
abstract interface ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity
}
