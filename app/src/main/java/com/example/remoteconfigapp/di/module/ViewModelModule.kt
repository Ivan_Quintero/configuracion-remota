package com.example.remoteconfigapp.di.module

import androidx.lifecycle.ViewModel
import com.example.remoteconfigapp.di.annotation.ViewModelKey
import com.example.remoteconfigapp.screens.main.viewmodel.MainViewModel
import com.example.remoteconfigapp.screens.splash.viewmodel.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel
}
