package com.example.remoteconfigapp.utils.timeouthelper

interface TimeOutListener {
    fun onLogoutSession()
}