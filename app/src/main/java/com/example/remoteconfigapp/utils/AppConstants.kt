package com.example.remoteconfigapp.utils

class AppConstants {

    companion object {
        const val API_GET_CERTIFICATES = "/vsshcore-server-security"
        const val API_EXCHANGE_KEY = "/vsshcore-exchange-key"
    }
}