package com.example.remoteconfigapp.screens.common.base

import androidx.lifecycle.ViewModelProvider
import com.example.remoteconfigapp.utils.timeouthelper.TimeOutHelper
import com.example.remoteconfigapp.utils.timeouthelper.TimeOutListener
import com.valid.utils.VsshLogger
import dagger.android.support.DaggerAppCompatActivity
import java.util.*
import javax.inject.Inject

open class BaseActivity : DaggerAppCompatActivity(), TimeOutListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var baseViewModel: BaseViewModel

    private companion object {
        val TAG = BaseActivity::class.java.simpleName
    }

    private var timerTask: TimerTask? = null

    fun setBaseViewModel(baseViewModel: BaseViewModel) {
        this.baseViewModel = baseViewModel
        this.observeBaseVariables()
    }

    private fun observeBaseVariables() {
        //TODO: Define Base variables
    }

    override fun onUserInteraction() {
        this.startUserSession()
        super.onUserInteraction()
    }

    private fun startUserSession() {
        if (TimeOutHelper.activeSession) {
            timerTask?.let {
                timerTask?.cancel()
            }

            if (TimeOutHelper.timer != null) {
                TimeOutHelper.timer?.cancel()
                TimeOutHelper.timer?.purge()
            }

            TimeOutHelper.timeOut?.let {
                timerTask = object : TimerTask() {
                    override fun run() {
                        onLogoutSession()
                    }
                }
                TimeOutHelper.timer = Timer()
                TimeOutHelper.timer?.schedule(timerTask, it)
            }
        }
    }

    override fun onLogoutSession() {
        VsshLogger.logDebug(TAG, "LOGOUT")
        TimeOutHelper.activeSession = false
        runOnUiThread { this.callTimeOutMain() }
    }

    private fun callTimeOutMain() {
        //TODO: show custom alert
    }
}
