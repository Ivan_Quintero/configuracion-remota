package com.example.remoteconfigapp.screens.main.view

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.remoteconfigapp.R
import com.example.remoteconfigapp.databinding.ActivityMainBinding
import com.example.remoteconfigapp.screens.common.base.BaseActivity
import com.example.remoteconfigapp.screens.main.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

         viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        //binding.viewModel = viewModel

        viewModel.write()
        viewModel.getMiniView()
        viewModel.getUrlInfo()
        viewModel.getVersionCode()
        observeViewModel()
    }

    fun observeViewModel(){
        viewModel.miniView.observe(this, Observer {
            Glide.with(this)
                .load(it)
                .into(miniview)
        })
        viewModel.versionCode.observe(this, Observer {
            val versionName: String = this.packageManager
                .getPackageInfo(this.packageName, 0).versionName
            if (it != versionName) showAlert(it)
        })
    }

    fun onMiniviewClick(view: View) {
        viewModel.urlInfo.observe(this, Observer {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
            startActivity(intent)
        })
    }

    fun showAlert(versionCode: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.update)
        builder.setMessage("Existe una nueva versión: $versionCode")
        builder.setIcon(android.R.drawable.ic_dialog_alert)
        builder.setPositiveButton(R.string.update){ _, _ ->
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store"))
            startActivity(intent)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
}
