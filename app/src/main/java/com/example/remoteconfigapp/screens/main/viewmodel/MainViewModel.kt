package com.example.remoteconfigapp.screens.main.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.example.remoteconfigapp.screens.common.base.BaseViewModel
import com.example.remoteconfigapp.screens.main.repository.MainRepository
import javax.inject.Inject

class MainViewModel @Inject constructor(private val mainRepository: MainRepository) :
    BaseViewModel() {
    val test = ObservableField<String>()
    var miniView: MutableLiveData<String> = MutableLiveData()
    var urlInfo: MutableLiveData<String> = MutableLiveData()
    var versionCode: MutableLiveData<String> = MutableLiveData()

    fun write() {
        test.set("test")

        mainRepository.setUp(null) {
            //Nothing
        }
    }

    fun getMiniView():String{
        miniView.value = mainRepository.getMiniView()
        return miniView.value!!
    }


    fun getUrlInfo():String{
        urlInfo.value = mainRepository.getUrlInfo()
        return urlInfo.value!!
    }

    fun getVersionCode(): String{
        versionCode.value = mainRepository.getVersionCode()
        return versionCode.value!!
    }
}
