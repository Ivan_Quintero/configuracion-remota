package com.example.remoteconfigapp.screens.splash.view

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.remoteconfigapp.R
import com.example.remoteconfigapp.databinding.ActivitySplashBinding
import com.example.remoteconfigapp.screens.common.base.BaseActivity
import com.example.remoteconfigapp.screens.main.view.MainActivity
import com.example.remoteconfigapp.screens.splash.viewmodel.SplashViewModel

class SplashActivity : BaseActivity() {
    private lateinit var viewModel: SplashViewModel
    lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_splash)

         viewModel= ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel::class.java)

        viewModel.init(this)

        viewModel.initFirebase()

        observeViewModel()

    }


    fun observeViewModel(){
        viewModel.isSetup.observe(this, Observer { isSetup ->
            if (isSetup) {

            }
        })

        viewModel.firebaseIsSetup.observe(this, Observer {isFirebaseSetup->
            if (isFirebaseSetup){
                isFirebaseSetup?.let {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            }
        })
    }
}
