package com.example.remoteconfigapp.screens.main.repository

import android.content.Context
import com.example.remoteconfigapp.BuildConfig
import com.example.remoteconfigapp.screens.common.base.BaseRepository
import com.example.remoteconfigapp.utils.AppConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.valid.vssh_android_core.VsshCoreManager
import com.valid.vssh_android_core.model.SetupModel
import javax.inject.Inject

class MainRepository @Inject constructor(private val coreManager: VsshCoreManager) :
    BaseRepository() {

    lateinit var isSetup: (value: Boolean) -> Unit

    fun setUp(context: Context?, isSetup: (value: Boolean) -> Unit) {
        this.isSetup = isSetup

        val setupModel = SetupModel()
        setupModel.context = context
        setupModel.urlBase = BuildConfig.BASE_URL
        setupModel.apiGetCertificates = AppConstants.API_GET_CERTIFICATES
        setupModel.apiExchange = AppConstants.API_EXCHANGE_KEY
        setupModel.publicKey = BuildConfig.PUBLIC_KEY
        coreManager.setup(
            setupModel,
            {
                this.isSetup(it)// en caso de que la conexion haya sido exitosa, devolveremos un valor de true a la variable que vamos a observar en nuestro viewmodel
            },
            {
                this.isSetup(false)
            }
        )
    }

    fun getMiniView(): String{
        return Firebase.remoteConfig.getString("miniview_image")
    }

    fun getUrlInfo(): String{
        return Firebase.remoteConfig.getString("url_access")
    }

    fun getVersionCode(): String{
        return Firebase.remoteConfig.getString("version_code")
    }
}
