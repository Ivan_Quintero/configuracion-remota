package com.example.remoteconfigapp.screens.splash.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.remoteconfigapp.screens.common.base.BaseViewModel
import com.example.remoteconfigapp.screens.splash.repository.SplashRepository
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val splashRepository: SplashRepository) :
    BaseViewModel() {

    var isSetup: MutableLiveData<Boolean> = MutableLiveData()
    var firebaseIsSetup: MutableLiveData<Boolean> = MutableLiveData()

    fun init(context: Context) {
        splashRepository.setUp(context) {
            isSetup.value = it
        }
    }

    fun initFirebase(){
        splashRepository.setUpFirebase {
            firebaseIsSetup.value = it
        }
    }
}
