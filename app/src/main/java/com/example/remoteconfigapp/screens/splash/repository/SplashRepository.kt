package com.example.remoteconfigapp.screens.splash.repository

import android.content.Context
import com.example.remoteconfigapp.BuildConfig
import com.example.remoteconfigapp.R
import com.example.remoteconfigapp.screens.common.base.BaseRepository
import com.example.remoteconfigapp.utils.AppConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.valid.vssh_android_core.VsshCoreManager
import com.valid.vssh_android_core.model.SetupModel
import javax.inject.Inject

class SplashRepository @Inject constructor(private val coreManager: VsshCoreManager, private val firebaseManager: FirebaseRemoteConfig) :
    BaseRepository() {

    lateinit var isSetup: (value: Boolean) -> Unit
    lateinit var firebaseIsSetup: (value: Boolean) -> Unit

    fun setUp(context: Context?, isSetup: (value: Boolean) -> Unit) {
        this.isSetup = isSetup

        val setupModel = SetupModel()
        setupModel.context = context
        setupModel.urlBase = BuildConfig.BASE_URL
        setupModel.apiGetCertificates = AppConstants.API_GET_CERTIFICATES
        setupModel.apiExchange = AppConstants.API_EXCHANGE_KEY
        setupModel.publicKey = BuildConfig.PUBLIC_KEY
        coreManager.setup(
            setupModel,
            {
                this.isSetup(it)
            },
            {
                this.isSetup(false)
            }
        )
    }
    fun setUpFirebase(firebaseIsSetup: (value: Boolean) -> Unit) {
        this.firebaseIsSetup = firebaseIsSetup

        firebaseManager.setDefaultsAsync(R.xml.remot_config_defaults)
        Firebase.remoteConfig.fetchAndActivate().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                this.firebaseIsSetup(true)
            } else this.firebaseIsSetup(false)
        }
    }
}
